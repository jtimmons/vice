;;; modifiers.el --- VI modifier functions for the VIce library

;; Copyright (C) 2017 Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Maintainer: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 5 Feb 2017

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:
;; This file is a part of the VIce library. See 'vice.el' for more general
;; library details and package information.
;;
;; modifiers.el contains the language specification of VI text object modifiers.
;; A mapping of possible modifier values to the functions that perform them is
;; contained in 'vice--modifier-table'. All other functions prefixed by
;; 'vice--modifier-*' define functions which perform the modification of a given
;; text object symbol.
;;
;; Each modifier function takes in a symbol which refers to a VIce text object
;; using the 'thingatpt' library and returns the bounds of that symbol after
;; applying the modifier to it. By default, the 'vice--modifier-none' function
;; should be used.
;;
;; See the documentation for each modifier function for more information.

;;; Code:

(defvar vice--modifier-table
  '((?i   . vice--modifier-inside)
    (?a   . vice--modifier-around)
    (nil  . vice--modifier-none))
      
  "A table mapping VIce modifiers to the functions which are used to apply them.
Each function in the table takes in a text object to operate on in the form of a
THING symbol (see `vice--text-object-table for possible symbols).

A default `vice--modifier-none function is provided for use when there is no
modifier specified.")


(defun vice--modifier-none (thing)
  "The default modifier to apply to VIce text objects.

Takes a symbol to a THING which is usable with `bounds-of-thing-at-point`.

By default it should go from the current point position to the end bound of the
text object. Take, for example, getting a 'word' in the scenario below:

    this is a sentence
                ^
The bounds of the entire word (if we were using the 'i' modifier) at the point would
be (11 . 18), but our definition for the default usage of 'word' goes from the current
point to the end of it - leaving the bounds as (13 . 18) 'ntence'.

Contrast this to a back-word, which has its bounds simply reversed. The bounds are (18 . 11),
which then becomes (13 . 11) 'se'."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (when bounds
      (cons (point) (cdr bounds)))))


(defun vice--modifier-inside (thing)
  "The 'inside' modifier to apply to VIce text objects.

Takes a symbol to a THING which is usable with `bounds-of-thing-at-point`.

Inside assumes that the point is currently inside of the specified text object;
contrary to the `vice--modifier-none` function, this simply returns the entire
bounds of the thing currently under the point. Take, for example, an 'inside word'
in the scenario below:

    this is a sentence
               ^ ^   ^
At any of the points shown above, the bounds of the 'word' will be (11 . 18) 'sentence'."
  (bounds-of-thing-at-point thing))


(defun vice--modifier-around (thing)
  "The 'around' modifier to apply to VIce text objects.

Takes a symbol to a THING which is usable with `bounds-of-thing-at-point`.

Around behaves similarly to `vice--modifier-inside`, except that it also selects
the whitespace around the text object on the side of the end boundary. Take, for
example, an 'around' word in the scenario below:

    this is a longer   sentence
                 ^
At any point in the word 'longer', the 'around word' will return (13 . 20) 'longer   '."
  (let* ((bounds    (bounds-of-thing-at-point thing))
	 (direction (if (< (car bounds) (cdr bounds)) 1 -1)))
    (save-excursion
      (goto-char (cdr bounds))
      (forward-whitespace direction)
      (cons (car bounds) (point)))))

;; modifiers.el ends here
