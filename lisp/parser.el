;;; parser.el --- VI language parsing functions for the VIce library

;; Copyright (C) 2017 Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Maintainer: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 5 Feb 2017

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:
;; This file is a part of the VIce library. See 'vice.el' for more general
;; library details and package information.
;;
;; parser.el contains functions for parsing VI commands - it relies on the
;; language definitions in the 'vice/lisp/lang' directory to determine the
;; character sets which are used for each VI language object type.
;;
;; The language is parsed using parser combinators created using the parsec
;; library. These parsers eventually rollup into the 'vice--parser-cmd' parser,
;; which can parse an entire VI command. This parser returns a list containing
;; the parsed results for each VI lanaguage object type. The list will be either:
;;
;;   (STANDALONE_VERB nil nil nil)
;;     OR
;;   (nil VERB MODIFIER TEXT_OBJECT)
;;
;; Each of these returned values can be used to index into the respective language
;; tables to perform the correct function on the correct text object.

;;; Code:

(require 'parsec)   ;; parsing functions
(require 'cl)       ;; remove-if

(defun vice--get-chars-from-table (table)
  (remove-if nil (mapcar 'car table)))

;; Basic parsers. Parse individual segments of a command
(defun vice--parser-text-object ()
  (apply #'parsec-one-of (vice--get-chars-from-table vice--text-object-table)))

(defun vice--parser-modifier ()
  (apply #'parsec-one-of (vice--get-chars-from-table vice--modifier-table)))

(defun vice--parser-verb ()
  (apply #'parsec-one-of (vice--get-chars-from-table vice--verb-table)))

(defun vice--parser-standalone-verb ()
  (apply #'parsec-one-of (vice--get-chars-from-table vice--standalone-verb-table)))


;; Command parsers. Parse an entire command.
(defun vice--parser-cmd-verb-with-text-object ()
  (parsec-collect
   nil                                      ;; Standalone-Verb
   (parsec-optional (vice--parser-verb))    ;; Verb
   nil                                      ;; Modifier
   (vice--parser-text-object)))             ;; Text-Object

(defun vice--parser-cmd-verb-with-modified-text-object ()
  (parsec-collect
   nil                                      ;; Standalone-Verb
   (vice--parser-verb)                      ;; Verb
   (vice--parser-modifier)                  ;; Modifier
   (vice--parser-text-object)))             ;; Text-Object

(defun vice--parser-cmd-standalone-verb ()
  (parsec-collect
   (vice--parser-standalone-verb)           ;; Standalone-Verb
   nil                                      ;; Verb
   nil                                      ;; Modifier
   nil))                                    ;; Text-Object

;; Putting it all together
(defun vice--parser-cmd ()
  (let ((case-fold-search nil)) ;; Case sensitivity

    (parsec-or
     (parsec-try (vice--parser-cmd-standalone-verb))
     (parsec-try (vice--parser-cmd-verb-with-text-object))
     (parsec-try (vice--parser-cmd-verb-with-modified-text-object)))))

(defun vice--parse-cmd (cmd)
  (let ((safe-string-to-char (lambda (c) (when c (string-to-char c))))
	(parsed-cmd (parsec-with-input cmd (vice--parser-cmd))))   ;; Parse `cmd`
    (mapcar safe-string-to-char parsed-cmd)))                      ;; Convert results to char list

;;; parser.el ends here
