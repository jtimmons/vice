
(require 'vice)

(require 'ert)
(require 'cl) ; reduce

(defun vice--lists-contain-common-values (list1 list2)
  (reduce (lambda (x y) (or x y))
	  (mapcar (lambda (element) (member element list2)) list1)))

(defun vice--assoc-lists-contain-common-keys (list1 list2)
  (vice--lists-contain-common-values
   (mapcar 'car vice--verb-table)
   (mapcar 'car vice--standalone-verb-table)))


(ert-deftest vice--mutually-exclusive-tables ()
  "Tests that each of the defined tables have mutually-exclusive values.

Tables which are used by OR'd command segments should be mutually exclusive. For
example, the entire command must start with either an entry in the standalone-verb
table OR either an entry in the verb table or an entry in the text-object table. For
that reason, it's ambiguous if ANY of those tables contain a common value.

  Standalone-Verb XOR Verb
  Standalone-Verb XOR Text-Object
  Verb            XOR Text-Object
"
  (should-not (vice--assoc-lists-contain-common-keys vice--standalone-verb-table vice--verb-table))
  (should-not (vice--assoc-lists-contain-common-keys vice--standalone-verb-table vice--text-object-table))
  (should-not (vice--assoc-lists-contain-common-keys vice--verb-table vice--text-object-table)))
