;;; text-objects.el --- Text Objects for the VIce library

;; Copyright (C) 2017 Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Maintainer: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 5 Feb 2017

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:
;; This file is a part of the VIce library. See 'vice.el' for more general
;; library details and package information.
;;
;; text-objects.el contains the language specification of VI text objects.
;; A mapping of possible text object values to the symbols that define them is
;; contained in 'vice--text-object-table'. All other symbols prefixed by
;; 'vice--text-object-*' define thingatpt THINGs for each text object.

;;; Code:

(require 'thingatpt)

(defvar vice--text-object-table
  '((?b . vice--back-word)
    (?w . vice--word)
    (?B . vice--back-Word)
    (?W . vice--Word)
    (?e . vice--end-word)
    (?E . vice--end-Word)
    (?s . vice--sentence)
    (?p . vice--paragraph)
    (?h . vice--char-left)
    (?j . vice--char-down)
    (?k . vice--char-up)
    (?l . vice--char-right)
    (?$ . vice--end-line)
    (?% . vice--matching-bracket)
    (?^ . vice--start-line-indent)
    (?G . vice--end-file)
    (?L . vice--bottom-screen)
    (?M . vice--middle-screen)
    (?H . vice--top-screen)
    (?f . vice--find)
    (?F . vice--back-find)
    (?t . vice--til)
    (?T . vice--back-til)
    (?/ . vice--search)
    (?? . vice--back-search))

  "A table mapping VIce Text Objects characters to the symbols which are used to
define them using `thing-at-point. The bounds of each THING in the table are defined
using the functions forward-<THING> (ex `vice--back-word).")

(defvar vice--word-sep-syntax-classes '(?_ ?. ?' ?( ?))
  "A list of syntax values from `char-syntax which are used to separate VIce 'word's.
See the documentation for `modify-syntax-entry for a list of all possible values.")

(defvar vice--last-search-string nil
  "The last string that was searched with the '/' command.")

(defun vice--forward-word (&optional arg)
  "Moves forward one word. ARG dictates what direction to move."
  (let ((direction (if (and arg (< arg 0)) -1 1)))
    (forward-char direction)
    (while (not
	    (or
	     ;; Separator in middle of word
	     (and (equal (char-syntax (char-before)) ?w)
		  (member (char-syntax (char-after)) vice--word-sep-syntax-classes))
	     ;; Start of word after whitespace or symbol
	     (and (or (equal (char-syntax (char-before)) ? )
		      (member (char-syntax (char-before)) vice--word-sep-syntax-classes))
		  (equal (char-syntax (char-after)) ?w))))
      (forward-char direction))))

(defun vice--backward-word (&optional arg)
  "Moves backward one word. ARG dictates what direction to move."
  (let ((neg-arg (if arg (* arg -1) -1)))
    (vice--forward-word neg-arg)))

(defun vice--forward-Word (&optional arg)
  "Moves forward one Word. ARG dictates what direction to move."
  (let ((direction (if (and arg (< arg 0)) -1 1)))
    (forward-char direction)
    (while (not (and (equal (char-syntax (char-before)) ? )
		     (not (equal (char-syntax (char-after)) ? ))))
      (forward-char direction))))

(defun vice--backward-Word (&optional arg)
  "Moves backward one word. ARG dictates what direction to move."
  (let ((neg-arg (if arg (* arg -1) -1)))
    (vice--forward-Word neg-arg)))

(defun vice--forward-end-word (&optional arg)
  (let ((direction (if (and arg (< arg 0)) -1 1)))
    (forward-char direction)
    (while (not
	    (or
	     ;; First symbol after whitespace (totally insane behavior, but it's correct)
	     (and
	      (equal (char-syntax (char-before)) ? )
	      (member (char-syntax (char-after)) vice--word-sep-syntax-classes))
	     ;; End of word, start of whitespace
	     (and
	      (equal (char-syntax (char-after)) ? )
	      (not (equal (char-syntax (char-before)) ? )))))
      (forward-char direction))))

(defun vice--forward-end-Word (&optional arg)
  (let ((direction (if (and arg (< arg 0)) -1 1)))
    (forward-char direction)
    (while (not (and
		 (equal (char-syntax (char-after)) ? )
		 (not (equal (char-syntax (char-before)) ? ))))
      (forward-char direction))))

(defun vice--forward-sentence (&optional arg)
  (let ((direction (if (and arg (< arg 0)) -1 1)))
    (forward-char direction)
    (while (not
	    (and
	     (equal (char-syntax (char-before)) ?.)
	     (equal (char-syntax (char-after)) ? )))
      (forward-char direction))))

(defun vice--forward-paragraph (&optional arg)
  (let ((direction (if (and arg (< arg 0)) -1 1)))
    (forward-char)
    (re-search-forward "\n *\t*\n" nil t direction)
    (search-backward "\n" nil t direction)
    (beginning-of-line)
    (backward-char direction)))

(defun vice--bounds-of-char-down ()
    (save-excursion
    (let ((start (point)))
      (next-line)
      (cons start (point)))))

(defun vice--bounds-of-char-up ()
  (save-excursion
    (let ((start (point)))
      (previous-line)
      (cons start (point)))))

(defun vice--bounds-of-end-line ()
  (cons (point) (line-end-position)))

(defun vice--forward-matching-bracket ()
  (let ((opening-bracket (equal (char-syntax (char-after)) ?())
	(closing-bracket (equal (char-syntax (char-before)) ?))))
    (cond
     (opening-bracket (forward-list))
     (closing-bracket (backward-list)))))

(defun vice--bounds-of-start-line-indent ()
  (save-excursion
    (let ((start (point)))
      (back-to-indentation)
      (cons start (point)))))

(defun vice--bounds-of-end-file ()
  (cons (point) (point-max)))

(defun vice--bounds-of-bottom-screen ()
  (save-excursion
    (let ((start (point)))
      (move-to-window-line -1)
      (cons start (point)))))

(defun vice--bounds-of-middle-screen ()
  (save-excursion
    (let ((start (point)))
      (move-to-window-line-top-bottom)
      (cons start (point)))))

(defun vice--bounds-of-top-screen ()
  (save-excursion
    (let ((start (point)))
      (move-to-window-line 0)
      (cons start (point)))))

(defun vice--bounds-of-find () nil) ;; TODO
(defun vice--bounds-of-back-find () nil) ;; TODO
(defun vice--bounds-of-til () nil) ;; TODO
(defun vice--bounds-of-back-til () nil) ;; TODO

(defun vice--bounds-of-search ()
  (setq vice--last-search-string (read-string "Search: "))
  (save-excursion
    (let ((start (point)))
      (search-forward vice--last-search-string)
      (cons start (point)))))

(defun vice--bounds-of-back-search ()
  (setq vice--last-search-string (read-string "Search: "))
  (save-excursion
    (let ((start (point)))
      (search-backward vice--last-search-string)
      (cons start (point)))))

;; Put all of the defined bounds functions into thingatpt properties lists,
;; This allows them to be used as THINGs (eg. (thing-at-point 'vice--word)).
;;
;; For this we're using `forward-op, which should move forward or backward
;; one THING according to a 'direction' argument, and `bounds-of-thing-at-point
;; which should just return the bounds of the THING.
(put 'vice--word              'forward-op  'vice--forward-word)
(put 'vice--back-word         'forward-op  'vice--backward-word)
(put 'vice--Word              'forward-op  'vice--forward-Word)
(put 'vice--back-Word         'forward-op  'vice--backward-Word)
(put 'vice--end-word          'forward-op  'vice--forward-end-word)
(put 'vice--end-Word          'forward-op  'vice--forward-end-Word)
(put 'vice--paragraph         'forward-op  'vice--forward-paragraph)
(put 'vice--matching-bracket  'forward-op  'vice--forward-matching-bracket)
(put 'vice--sentence          'forward-op  'vice--forward-sentence)
(put 'vice--char-left         'forward-op  'forward-char)
(put 'vice--char-right        'forward-op  'backward-char)

(put 'vice--char-down         'bounds-of-thing-at-point 'vice--bounds-of-char-down)
(put 'vice--char-up           'bounds-of-thing-at-point 'vice--bounds-of-char-up)
(put 'vice--end-line          'bounds-of-thing-at-point 'vice--bounds-of-end-line)
(put 'vice--start-line-indent 'bounds-of-thing-at-point 'vice--bounds-of-start-line-indent)
(put 'vice--end-file          'bounds-of-thing-at-point 'vice--bounds-of-end-file)
(put 'vice--bottom-screen     'bounds-of-thing-at-point 'vice--bounds-of-bottom-screen)
(put 'vice--middle-screen     'bounds-of-thing-at-point 'vice--bounds-of-middle-screen)
(put 'vice--top-screen        'bounds-of-thing-at-point 'vice--bounds-of-top-screen)
(put 'vice--find              'bounds-of-thing-at-point 'vice--bounds-of-find)
(put 'vice--back-find         'bounds-of-thing-at-point 'vice--bounds-of-back-find)
(put 'vice--til               'bounds-of-thing-at-point 'vice--bounds-of-til)
(put 'vice--back-til          'bounds-of-thing-at-point 'vice--bounds-of-back-til)
(put 'vice--search            'bounds-of-thing-at-point 'vice--bounds-of-search)
(put 'vice--back-search       'bounds-of-thing-at-point 'vice--bounds-of-back-search)

;; text-objects.el ends here
