#+STARTUP: showeverything

* Overview
VIce is an emacs library for parsing and handling vim "normal mode" commands
as their own language. There is no concept of modes in VIce - it isn't meant
to be a VI *emulator* (see: vim-mode, viper, evil) as much is it's meant
to provide the expressive functionality and more importantly *composeability*
of VI commands to a common emacs interface.

See the [[./doc/lang.org][language documentation]] for the specification of the VI language and
how it is parsed and represented internally.

* TODOs

** TODO Language Spec
  - [X] Create initial language specification
  - [X] Stub out language code (figure out function types for each + map)
  - [X] Implement verbs
    - [X] Implement 'change'
    - [X] Implement 'delete'
    - [X] Implement 'yank'
    - [X] Implement 'visual'
  - [X] Implement modifiers
    - [X] Implement default
    - [X] Implement 'inside'
    - [X] Implement 'around'
  - [-] Implement text objects
    - [X] Implement 'back-word'
    - [X] Implement 'word'
    - [X] Implement 'back-Word'
    - [X] Implement 'Word'
    - [X] Implement 'end-word'
    - [X] Implement 'end-Word'
    - [X] Implement 'sentence'
    - [X] Implement 'paragraph'
    - [X] Implement 'char-left'
    - [X] Implement 'char-down'
    - [X] Implement 'char-up'
    - [X] Implement 'char-right'
    - [X] Implement 'end-line'
    - [X] Implement 'matching-bracket'
    - [X] Implement 'start-line-indent'
    - [X] Implement 'end-file'
    - [X] Implement 'bottom-screen'
    - [X] Implement 'middle-screen'
    - [X] Implement 'top-screen'
    - [ ] Implement 'find'
    - [ ] Implement 'back-find'
    - [ ] Implement 'til'
    - [ ] Implement 'back-til'
    - [ ] Implement 'search'
    - [ ] Implement 'back-search'
  - [X] Implement standalone verbs
    - [X] Implement 'after'
    - [X] Implement 'insert-mode'
    - [X] Implement 'open-line'
    - [X] Implement 'paste'
    - [X] Implement 'replace'
    - [X] Implement 'substitute'
    - [X] Implement 'undo'
    - [X] Implement 'delete-char'
    - [X] Implement 'after-line'
    - [X] Implement 'change-to-end-of-line'
    - [X] Implement 'delete-to-end-of-line'
    - [X] Implement 'insert-after-whitespace'
    - [X] Implement 'join-line'
    - [X] Implement 'open-line-above'
    - [X] Implement 'paste-before-point'
    - [X] Implement 'replace-mode'
    - [X] Implement 'substitute-line'
    - [X] Implement 'delete-char-backwards'
    - [X] Implement 'yank-line'

** TODO Parser
  - [X] Design parser (parsec?)
  - [X] Implement parser
  - [X] Implement 'run' command to join language and parser

** TODO Additional Design Work
  - [ ] Consider breaking 'text object's up into 'modifiable' and 'unmodifiable'. I hit
    a bit of a snag on things like 'e' (end-of-word), which cannot be modified in
    VI/VIM. It technically works in the current implementation, but is kind of odd.

    There's also some contextual text object characters like 'b', which is 'back-word'
    when unmodified, and 'block' when modified.

  - [ ] Figure out how to do stream-based parsing. Want to be able to parse characters
    as they are typed by the user. It looks like this will be difficult with to do
    using the parsec library based on how parsing is implemented (eg. it dumps text
    to a temporary buffer and each parser "consumes" the text by moving the point).

** TODO General Fixes & Incorrect Behavior
  - [ ] Rewrite text-objects to extract out the common theme of "save-excursion, move somewhere,
    cons the previous point to the current point".

  - [ ] Search functions (ie. '/' '?') are not incremental. Should probably see
    if we can wrap 'isearch' and rebind the keybindings to be VIM-style.

  - [X] Rework all four word functions. Each should always move to the BEGINNING of the
    next word - past any whitespace.
