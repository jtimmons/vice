;;; vice.el --- VI language parsing and functions for emacs

;; Copyright (C) 2017 Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Maintainer: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 5 Feb 2017

;; Keywords:
;; Homepage: https://gitlab.com/jtimmons/vice

;; Package-Version: 0.1
;; Package-Requires: ((parsec "0.1.3"))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:
;; VIce is an emacs library for parsing and handling vim "normal mode" commands
;; as their own language. There is no concept of modes in VIce - it isn't meant
;; to be a VI *emulator* (see: vim-mode, viper, evil) as much is it's meant
;; to provide the expressive functionality and more importantly *composeability*
;; of VI commands to a common emacs interface.
;;
;; VIce's functionality is exposed through a single interactive function:
;;
;;   M-x vice--run-cmd
;;
;; Full VI command strings passed to this function will be evaluated as expected.
;; For example, M-x vice--run-cmd dw RET will delete the next word.

;;; Code:

(load-file "./lang/modifiers.el")
(load-file "./lang/standalone-verbs.el")
(load-file "./lang/text-objects.el")
(load-file "./lang/verbs.el")

(load-file "./parser.el")

(defun vice--run-cmd (cmd)
  (interactive "sCommand: ")

  (let ((parsed-cmd (vice--parse-cmd cmd))) ;; Parse command

    ;; Unpack command - lookup in respective tables
    (let ((standalone-verb (alist-get (nth 0 parsed-cmd) vice--standalone-verb-table))
	  (verb            (alist-get (nth 1 parsed-cmd) vice--verb-table))
	  (modifier        (alist-get (nth 2 parsed-cmd) vice--modifier-table))
	  (text-object     (alist-get (nth 3 parsed-cmd) vice--text-object-table)))

      ;; Default to using standalone-verb if provided, otherwise apply verb to text-object
      (if standalone-verb
	  (funcall standalone-verb)
	(funcall verb (funcall modifier text-object))))))

(provide 'vice)

;;; vice.el ends here
