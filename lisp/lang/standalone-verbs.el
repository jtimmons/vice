;;; standalone-verbs.el --- Standalone Verb functions for the VIce library

;; Copyright (C) 2017 Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Maintainer: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 5 Feb 2017

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:
;; This file is a part of the VIce library. See 'vice.el' for more general
;; library details and package information.
;;
;; standalone-verbs.el contains the language specification of VI standalone verbs.
;; A mapping of possible standalone-verb values to the functions that perform them
;; is contained in 'vice--standalone-verb-table'. All other functions prefixed by
;; 'vice--standalone-verb-*' define functions which perform individual, one-off
;; VI verbs which do not act on a particular text object.
;;
;; See the documentation for each standalone-verb function for more information.

;;; Code:

(defvar vice--standalone-verb-table
  '((?a . vice--standalone-verb-after)
    (?i . vice--standalone-verb-insert-mode)
    (?o . vice--standalone-verb-open-line)
    (?p . vice--standalone-verb-paste)
    (?r . vice--standalone-verb-replace)
    (?s . vice--standalone-verb-substitute)
    (?u . vice--standalone-verb-undo)
    (?x . vice--standalone-verb-delete-char)
    (?A . vice--standalone-verb-after-line)
    (?C . vice--standalone-verb-change-to-end-of-line)
    (?D . vice--standalone-verb-delete-to-end-of-line)
    (?I . vice--standalone-verb-insert-after-whitespace)
    (?J . vice--standalone-verb-join-line)
    (?O . vice--standalone-verb-open-line-above)
    (?P . vice--standalone-verb-paste-before-point)
    (?R . vice--standalone-verb-replace-mode)
    (?S . vice--standalone-verb-substitute-line)
    (?X . vice--standalone-verb-delete-char-backwards)
    (?Y . vice--standalone-verb-yank-line))

  "A table mapping VIce standalone verbs to the functions which are used to
evaluate them. Each function takes no arguments and performs operations based
on the editor's context.")

(defun vice--standalone-verb-after ()
  (forward-char))

(defun vice--standalone-verb-insert-mode ()
  (let ((prefix-arg 1))
    (call-interactively 'overwrite-mode)))

(defun vice--standalone-verb-open-line ()
  (end-of-line)
  (newline-and-indent))

(defun vice--standalone-verb-paste ()
  (forward-char)
  (yank))

(defun vice--standalone-verb-replace ()
  (let ((new-char (read-char "New Char:")))
    (delete-forward-char 1)
    (insert-char new-char)))

(defun vice--standalone-verb-substitute ()
  (delete-forward-char 1)) ;; Already in 'insert' mode

(defun vice--standalone-verb-undo ()
  (undo))

(defun vice--standalone-verb-delete-char ()
  (delete-forward-char 1))

(defun vice--standalone-verb-after-line ()
  (end-of-line))

(defun vice--standalone-verb-change-to-end-of-line ()
  (delete-region (point) (line-end-position)))

(defun vice--standalone-verb-delete-to-end-of-line ()
  (kill-line))

(defun vice--standalone-verb-insert-after-whitespace ()
  (forward-whitespace 1))

(defun vice--standalone-verb-join-line ()
  (join-line))

(defun vice--standalone-verb-open-line-above ()
  (previous-line)
  (end-of-line)
  (newline-and-indent))

(defun vice--standalone-verb-paste-before-point ()
  (yank))

(defun vice--standalone-verb-replace-mode ()
  (let ((prefix-arg 1))
    (call-interactively 'overwrite-mode)))

(defun vice--standalone-verb-substitute-line ()
  (delete-region (line-beginning-position) (line-end-position)))

(defun vice--standalone-verb-delete-char-backwards ()
  (delete-backward-char 1))

(defun vice--standalone-verb-yank-line ()
  (kill-ring-save (point) (line-end-position)))

;; standalone-verbs.el ends here
