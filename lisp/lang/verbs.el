;;; verbs.el --- Verb functions for the VIce library

;; Copyright (C) 2017 Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Maintainer: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 5 Feb 2017

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301, USA.

;;; Commentary:
;; This file is a part of the VIce library. See 'vice.el' for more general
;; library details and package information.
;;
;; verbs.el contains the language specification of VI verbs. A mapping of possible
;; verb values to the functions that perform them is contained in 'vice--verb-table'.
;; All other functions prefixed by 'vice--verb-*' define functions which perform the
;; verb's action on a text object.
;;
;; Each verb function takes in a tuple which refers to the bounds of the VIce text
;; object, and performs the verb's action on the specified text object. By default,
;; the 'vice--verb-none' function should be used.
;;
;; See the documentation for each modifier function for more information.

;;; Code:

(defvar vice--verb-table
  '((?c  . vice--verb-change)
    (?d  . vice--verb-delete)
    (?y  . vice--verb-yank)
    (?v  . vice--verb-visual)
    (nil . vice--verb-none))
      
  "A table mapping VIce verbs to the functions which are used to evaluate them.
Each function in the table takes in a text object to operate on in the form of a
cons cell (START . END) giving the start end end positions of the text object.")


(defun vice--verb-change (bounds)
  "Apply a VIce 'change' command to the given text object.

Takes a cons-cell of character boundaries of the form (start, end).

The 'change' command takes the VI language meaning in this context, however, since
emacs doesn't have the concept of editing modes, the object is simply deleted."
  (when bounds
    (delete-region (car bounds) (cdr bounds))))

(defun vice--verb-delete (bounds)
  "Apply a VIce 'delete' command to the given text object.

Takes a cons-cell of character boundaries of the form (start, end).

The 'delete' command takes the VI language meaning in this context and so, unlike
in emacs, the object is removed and added to the kill ring. In emacs terminology,
this is a kill."
  (when bounds
    (kill-region (car bounds) (cdr bounds))))

(defun vice--verb-yank (bounds)
  "Apply a VIce 'yank' command to the given text object.

Takes a cons-cell of character boundaries of the form (start, end).

The 'yank' command takes the VI language meaning in this context and so, unlike
in emacs, the object is just added to the kill ring. In emacs terminology, this is
a kill-ring-save."
  (when bounds
    (kill-ring-save (car bounds) (cdr bounds))))

(defun vice--verb-visual (bounds)
  "Apply a VIce 'visual' command to the given text object.

Takes a cons-cell of character boundaries of the form (start, end).

The 'visual' command takes the VI language meaning in this context, meaning that
it would normally enter Visual mode with the text object selected. However, since
emacs doesn't have the concept of editing modes, the object is simply selected as normal."
  (when bounds
    (progn (push-mark (car bounds) t t)
	   (goto-char (cdr bounds)))))

(defun vice--verb-none (bounds)
  "The default verb to apply to VIce text objects, movement.

Takes a cons-cell of character boundaries of the form (start, end).

The default action to take is to move to the end of the text object's boundaries,
for example, the command 'w' moves to the end of a 'word'."
  (when bounds
    (goto-char (cdr bounds))))

;; verbs.el ends here
